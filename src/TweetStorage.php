<?php

namespace Drupal\tweet_reference;

use Drupal\Core\Database\Connection;

/**
 * Class TweetStorage.
 */
class TweetStorage {

  const TWEET_TABLE = 'tweet_reference';
  const FEED_TABLE = 'tweet_reference_feed';
  const USER_TABLE = 'tweet_reference_user';

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Constructs a new TweetStorage object.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   */
  public function __construct(Connection $database) {
    $this->database = $database;
  }

  /**
   * Loads tweet records synced before the specified time.
   */
  public function loadOutdatedTweetRecords($age, $limit) {
    return $this->database->select(static::TWEET_TABLE, 't')->fields('t')->condition('t.synced_time', $age, '<')->range(0, $limit)->execute()->fetchAllAssoc('id', \PDO::FETCH_ASSOC);
  }

  /**
   * Loads tweet records by identifier.
   */
  public function loadExtTweetRecordsById($ids) {
    if (empty($ids)) {
      return [];
    }

    $query = $this->database->select(static::TWEET_TABLE, 't');
    $query->fields('t');
    $query->condition('t.id', $ids, 'IN');
    $query->leftJoin(static::USER_TABLE, 'u', 'u.user_id = t.user_id');
    $query->addField('u', 'fid', 'user_fid');

    $tweets = $query->execute()->fetchAllAssoc('id', \PDO::FETCH_ASSOC);

    foreach ($tweets as &$tweet) {
      if (!is_null($tweet['tweet_data'])) {
        $tweet['tweet_data'] = unserialize($tweet['tweet_data']);
      }
    }

    return $tweets;
  }

  /**
   * Loads tweet records by tweet identifier.
   */
  public function loadTweetRecordsByTweetId($tweet_id) {
    $tweets = $this->database->select(static::TWEET_TABLE, 't')->fields('t')->condition('t.tweet_id', $tweet_id)->execute()->fetchAllAssoc('id', \PDO::FETCH_ASSOC);

    foreach ($tweets as &$tweet) {
      if (!is_null($tweet['tweet_data'])) {
        $tweet['tweet_data'] = unserialize($tweet['tweet_data']);
      }
    }

    return $tweets;
  }

  /**
   * Loads tweet records by identifier.
   */
  public function loadUserRecordById($user_id) {
    $users = $this->database->select(static::USER_TABLE, 'u')->fields('u')->condition('u.user_id', $user_id)->execute()->fetchAllAssoc('user_id', \PDO::FETCH_ASSOC);

    foreach ($users as &$user) {
      if (!is_null($user['user_data'])) {
        $user['user_data'] = unserialize($user['user_data']);

        return $user;
      }
    }

    return NULL;
  }

  /**
   * Saves tweet record.
   */
  public function saveTweetRecord($tweet) {
    if (!is_null($tweet['tweet_data'])) {
      $tweet['tweet_data'] = serialize($tweet['tweet_data']);
    }

    $id = $tweet['id'];
    unset($tweet['id']);

    $this->database->update(static::TWEET_TABLE)->fields($tweet)->condition('id', $id)->execute();
  }

  /**
   * Creates new tweet record.
   */
  public function createEmptyTweetRecordIfNotExists($id, $tweet_id) {
    if (!empty($id) && $this->tweetRecordExists(['id' => $id])) {
      return $id;
    }

    return $this->database->insert(static::TWEET_TABLE)->fields(['tweet_id' => $tweet_id])->execute();
  }

  /**
   * Updates tweet record.
   */
  public function updateTweetRecord($id, $tweet_id, $entity_type, $entity_id, $check_user, $check_counts, $download_tweet = FALSE) {
    $fields = [
      'tweet_id' => $tweet_id,
      'entity_type' => $entity_type,
      'entity_id' => $entity_id,
      'check_user' => intval($check_user),
      'check_counts' => intval($check_counts),
    ];

    $dirty = FALSE;
    if (!$this->tweetRecordExists(['id' => $id, 'tweet_id' => $tweet_id], TRUE)) {
      // The tweet record changed the tweet id, re-sync it.
      $dirty = TRUE;
      $this->freeTweetRecordUser($id);
      $this->freeTweetRecordFile($id);
      $fields['user_id'] = '';
      $fields['synced_time'] = 0;
      $fields['fids'] = NULL;
      $fields['tweet_data'] = NULL;
    }

    $this->database->update(static::TWEET_TABLE)->fields($fields)->condition('id', $id)->execute();

    if ($dirty && $download_tweet) {
      \Drupal::service('tweet_reference.tweet_sync')->syncSingleTweet($tweet_id);
    }
  }

  /**
   * Deletes the tweet record.
   */
  public function deleteTweetRecord($id) {
    $this->freeTweetRecordUser($id);
    $this->freeTweetRecordFile($id);
    $this->database->delete(static::TWEET_TABLE)->condition('id', $id)->execute();
  }

  /**
   * Deletes all tweet record for the entity.
   */
  public function deleteTweetRecordsForEntity($entity_type, $entity_id) {
    $ids = $this->database->select(static::TWEET_TABLE, 't')->fields('t', ['id'])->condition('t.entity_type', $entity_type)->condition('t.entity_id', $entity_id)->execute()->fetchCol();

    foreach ($ids as $id) {
      $this->freeTweetRecordUser($id);
      $this->freeTweetRecordFile($id);
    }

    $this->database->delete(static::TWEET_TABLE)->condition('entity_type', $entity_type)->condition('entity_id', $entity_id)->execute();
  }

  /**
   * Deletes all tweet record for the entity.
   */
  public function deleteTweetRecords($ids) {
    if (empty($ids)) {
      return;
    }

    $ids = $this->database->select(static::TWEET_TABLE, 't')->fields('t', ['id'])->condition('t.id', $ids, 'IN')->execute()->fetchCol();

    if (empty($ids)) {
      return;
    }

    foreach ($ids as $id) {
      $this->freeTweetRecordUser($id);
      $this->freeTweetRecordFile($id);
    }

    $this->database->delete(static::TWEET_TABLE)->condition('id', $ids, 'IN')->execute();
  }

  /**
   * Checks if a record which meets conditions exists.
   */
  private function tweetRecordExists($conditions, $check_synced = FALSE) {
    $query = $this->database->select(static::TWEET_TABLE, 't')->fields('t', ['id']);

    foreach ($conditions as $field => $value) {
      $query->condition('t.'. $field, $value);
    }

    if ($check_synced) {
      $query->condition('synced_time', 0, '>');
    }

    $id = $query->execute()->fetchField();

    return !empty($id);
  }

  /**
   * Creates new Twitter user record.
   */
  public function  createUserRecord($user) {
    if (!is_null($user['user_data'])) {
      $user['user_data'] = serialize($user['user_data']);
    }

    $this->database->insert(static::USER_TABLE)->fields($user)->execute();
  }

  /**
   * Updates Twitter user record.
   */
  public function  updateUserRecord($user) {
    if (!is_null($user['user_data'])) {
      $user['user_data'] = serialize($user['user_data']);
    }

    $user_id = $user['user_id'];
    unset($user['user_id']);

    $existing_user_fid = $this->database->select(static::USER_TABLE, 'u')->fields('u', ['fid'])->condition('u.user_id', $user_id)->execute()->fetchField();

    if (!empty($existing_user_fid) && (intval($existing_user_fid) !== intval($user['fid']))) {
      $file = \Drupal::entityTypeManager()->getStorage('file')->load($existing_user_fid);

      if (!empty($file)) {
        \Drupal::service('file.usage')->delete($file, 'tweet_reference', 'tweet_reference_user', $user_id);
      }
    }

    $this->database->update(static::USER_TABLE)->fields($user)->condition('user_id', $user_id)->execute();
  }

  /**
   * Returns the list of cache tags for entities referencing user's tweets.
   */
  public function getTweetEntityCacheTagsByUser($user_id) {
    $tweets = $this->database->select(static::TWEET_TABLE, 't')->fields('t', ['id', 'entity_type', 'entity_id'])->condition('t.user_id', $user_id)->execute()->fetchAllAssoc('id', \PDO::FETCH_ASSOC);
    $cache_tags = [];

    foreach ($tweets as $tweet) {
      if (!empty($tweet['entity_type']) && !empty($tweet['entity_id'])) {
        $cache_tag = $tweet['entity_type'] . ':' . $tweet['entity_id'];
        $cache_tags[$cache_tag] = $cache_tag;
      }
    }

    return array_values($cache_tags);
  }

  /**
   * Deletes tweet user record if the tweet is the only tweet of that user.
   */
  public function freeTweetRecordUser($id) {
    $user_id = $this->database->select(static::TWEET_TABLE, 't')->fields('t', ['user_id'])->condition('t.id', $id)->execute()->fetchField();

    if (empty($user_id)) {
      return;
    }

    $tweet_count = $this->database->select(static::TWEET_TABLE, 't')->fields('t', ['id'])->condition('t.user_id', $user_id)->countQuery()->execute()->fetchField();

    if ($tweet_count > 1) {
      return;
    }

    $user_fid = $this->database->select(static::USER_TABLE, 'u')->fields('u', ['fid'])->condition('u.user_id', $user_id)->execute()->fetchField();

    if (!empty($user_fid)) {
      $file = \Drupal::entityTypeManager()->getStorage('file')->load($user_fid);

      if (!empty($file)) {
        \Drupal::service('file.usage')->delete($file, 'tweet_reference', 'tweet_reference_user', $user_id);
      }
    }

    $this->database->delete(static::USER_TABLE)->condition('user_id', $user_id)->execute();
  }

  /**
   * Decrements tweet file usage counter.
   */
  public function freeTweetRecordFile($id) {
    $fids = $this->database->select(static::TWEET_TABLE, 't')->fields('t', ['fids'])->condition('t.id', $id)->execute()->fetchField();

    if (empty($fids)) {
      return;
    }

    foreach (explode(',', $fids) as $fid) {
      $fid = intval(trim($fid, ' []'));

      if (empty($fid)) {
        continue;
      }

      $file = \Drupal::entityTypeManager()->getStorage('file')->load($fid);

      if (!empty($file)) {
        \Drupal::service('file.usage')->delete($file, 'tweet_reference', 'tweet_reference', $id);
      }
    }
  }

}
