<?php

namespace Drupal\tweet_reference\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a form that configures Twitter api keys.
 */
class TwitterSettingsForm extends ConfigFormBase {

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * Constructs a TwitterSettingsForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, DateFormatterInterface $date_formatter) {
    parent::__construct($config_factory);

    $this->dateFormatter = $date_formatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('date.formatter')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'tweet_reference_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'tweet_reference.twitter_api',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('tweet_reference.twitter_api');

    $form['twitter_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Twitter consumer key'),
      '#default_value' => $config->get('twitter_key'),
      '#required' => TRUE,
    ];

    $form['twitter_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Twitter consumer secret'),
      '#default_value' => $config->get('twitter_secret'),
      '#required' => TRUE,
    ];

    $period = [172800, 432000, 864000];
    $period = array_map([$this->dateFormatter, 'formatInterval'], array_combine($period, $period));

    $form['cache_lifetime'] = [
      '#type' => 'select',
      '#title' => $this->t('Tweet cache lifetime'),
      '#description' => $this->t('The interval of refreshing of single tweets.'),
      '#options' => $period,
      '#default_value' => $config->get('cache_lifetime') ?: 172800,
      '#required' => TRUE,
    ];

    $period = [86400, 172800, 432000, 864000];
    $period = array_map([$this->dateFormatter, 'formatInterval'], array_combine($period, $period));

    $form['list_cache_lifetime'] = [
      '#type' => 'select',
      '#title' => $this->t('Tweet list cache lifetime'),
      '#description' => $this->t('The interval of refreshing of tweet lists.'),
      '#options' => $period,
      '#default_value' => $config->get('list_cache_lifetime') ?: 86400,
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('tweet_reference.twitter_api')
      ->set('twitter_key', $form_state->getValue('twitter_key'))
      ->set('twitter_secret', $form_state->getValue('twitter_secret'))
      ->set('cache_lifetime', $form_state->getValue('cache_lifetime'))
      ->set('list_cache_lifetime', $form_state->getValue('list_cache_lifetime'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
