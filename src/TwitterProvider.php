<?php

namespace Drupal\tweet_reference;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use GuzzleHttp\ClientInterface;

/**
 * Class TwitterProvider.
 */
class TwitterProvider {

  use LoggerChannelTrait;

  const TW_API_URL = 'https://api.twitter.com';
  const TW_API_VERS = '1.1';

  /**
   * The configuration object.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * The HTTP client to fetch the feed data with.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The cache backend to use.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cacheBackend;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * Constructs a new TwitterPostsBlock object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The HTTP client.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   Date Formatter.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   The cache backend to use.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, ClientInterface $http_client, DateFormatterInterface $date_formatter, CacheBackendInterface $cache_backend, TimeInterface $time) {
    $this->config = $config_factory->get('tweet_reference.twitter_api');
    $this->httpClient = $http_client;
    $this->dateFormatter = $date_formatter;
    $this->cacheBackend = $cache_backend;
    $this->time = $time;
  }

  /**
   * Get the latest tweets.
   */
  public function getTweets($key, $secret, $tweet_ids) {
    if (empty($tweet_ids)) {
      return [];
    }

    if (empty($key) || empty($secret)) {
      return NULL;
    }

    $token = $this->getAccessToken($key, $secret);

    if (empty($token)) {
      return NULL;
    }

    $url = $this->getResourceApiUrl() . '/statuses/lookup.json?tweet_mode=extended&include_entities=true&map=false&id=' . implode(',', $tweet_ids);
    $posts = $this->getResponse($url, $token);

    if (is_null($posts)) {
      return NULL;
    }

    return $posts;
  }

  /**
   * Get access token.
   */
  protected function getAccessToken($key, $secret) {
    static $tokens = NULL;

    if (empty($tokens)) {
      $tokens = [];
    }

    if (empty($tokens[$key])) {
      $url = $this->getAuthApiUrl();
      $app_id = urlencode($key);
      $secret_id = urlencode($secret);
      $basic = base64_encode("$app_id:$secret_id");

      try {
        $response = $this->httpClient->post(
          $url,
          [
            'headers' => [
              'Authorization' => "Basic $basic",
              'Content-Type' => 'application/x-www-form-urlencoded;charset=UTF-8',
            ],
          ]
        );
        $tokens[$key] = Json::decode($response->getBody());
      }
      catch (\Exception $exception) {
        $this->getLogger('tweet_reference')->error($exception->getMessage());
        return '';
      }
    }

    return !empty($tokens[$key]['access_token']) ? $tokens[$key]['access_token'] : '';
  }

  /**
   * Get response.
   */
  protected function getResponse($url, $token) {
    try {
      $response = $this->httpClient->get(
        $url,
        [
          'headers' => [
            'Authorization' => "Bearer $token",
          ],
        ]
      );

      if ($response->getStatusCode() != 200) {
        return NULL;
      }

      return Json::decode($response->getBody());
    }
    catch (\Exception $exception) {
      $this->getLogger('tweet_reference')->error($exception->getMessage());
      return NULL;
    }
  }

  /**
   * Help function for sort posts.
   */
  protected function postsSort($a, $b) {
    return $a['publication_time'] < $b['publication_time'];
  }

  /**
   * Get resource API url.
   */
  protected function getResourceApiUrl() {
    return self::TW_API_URL . '/' . self::TW_API_VERS;
  }

  /**
   * Get auth API url.
   */
  protected function getAuthApiUrl() {
    return self::TW_API_URL . '/oauth2/token?grant_type=client_credentials';
  }

}
