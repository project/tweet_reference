<?php

namespace Drupal\tweet_reference;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Component\Utility\Html;

/**
 * Class TweetSync.
 */
class TweetSync {

  /**
   * The configuration object.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * The tweet storage service.
   *
   * @var \Drupal\tweet_reference\TweetStorage
   */
  protected $tweetStorage;

  /**
   * The tweet storage service.
   *
   * @var \Drupal\tweet_reference\TwitterProvider
   */
  protected $twitterProvider;

  /**
   * The identifiers of user records updated in the current request.
   *
   * @var array
   */
  protected $updatedUsers = [];

  /**
   * Constructs a new TweetStorage object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   * @param \Drupal\tweet_reference\TweetStorage $tweet_storage
   *   The tweet storage service.
   * @param \Drupal\tweet_reference\TwitterProvider $twitter_provider
   *   The tweet storage service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, TweetStorage $tweet_storage, TwitterProvider $twitter_provider) {
    $this->config = $config_factory->get('tweet_reference.twitter_api');
    $this->tweetStorage = $tweet_storage;
    $this->twitterProvider = $twitter_provider;
  }

  /**
   * Updates tweets and other data by cron.
   */
  public function cronWorker() {
    $lifetime = intval($this->config->get('cache_lifetime'));
    if (empty($lifetime)) {
      $lifetime = 172800;
    }

    $current_time = time();

    $tweets = $this->tweetStorage->loadOutdatedTweetRecords($current_time - $lifetime, 80);

    if (empty($tweets)) {
      return;
    }

    $tweet_ids = [];

    foreach ($tweets as $tweet) {
      $tweet_id = strval($tweet['tweet_id']);
      if (!in_array($tweet_id, $tweet_ids, TRUE)) {
        $tweet_ids[] = $tweet_id;
      }
    }

    $this->fetchAndUpdateTweets($tweet_ids, $current_time);
  }

  /**
   * Updates single tweet.
   */
  public function syncSingleTweet($tweet_id) {
    $this->fetchAndUpdateTweets([$tweet_id], time());
  }

  /**
   * Fetches and updates tweets.
   */
  protected function fetchAndUpdateTweets($tweet_ids, $current_time) {
    $remote_tweets = $this->twitterProvider->getTweets($this->config->get('twitter_key'), $this->config->get('twitter_secret'), $tweet_ids);

    if (is_null($remote_tweets)) {
      return;
    }

    $remote_tweet_ids = [];
    $cache_tags = [];

    foreach ($remote_tweets as $remote_tweet) {
      $tweet_id = strval($remote_tweet['id_str']);
      $remote_tweet_ids[] = $tweet_id;
      $this->updateTweetsByTweetId($tweet_id, $remote_tweet, $current_time, $cache_tags);
    }

    foreach ($tweet_ids as $tweet_id) {
      if (!in_array($tweet_id, $remote_tweet_ids, TRUE)) {
        $this->deleteTweetsByTweetId($tweet_id, $cache_tags);
      }
    }

    Cache::invalidateTags(array_values($cache_tags));
  }

  /**
   * Updates tweets having common tweet identifier.
   */
  protected function updateTweetsByTweetId($tweet_id, $remote_tweet, $current_time, &$cache_tags) {
    $tweets = $this->tweetStorage->loadTweetRecordsByTweetId($tweet_id);
    // $remote_tweet_data = $remote_tweet;
    $real_remote_tweet = $remote_tweet;

    if (!empty($remote_tweet['retweeted_status'])) {
      // Take the original tweet from the retweet.
      $real_remote_tweet = $remote_tweet['retweeted_status'];
    }

    $user_id = strval($real_remote_tweet['user']['id_str']);

    if (!in_array($user_id, $this->updatedUsers, TRUE)) {
      $this->updatedUsers[] = $user_id;
      $this->updateUserRecord($user_id, $real_remote_tweet['user'], $current_time, $cache_tags);
    }

    foreach ($tweets as $tweet) {
      // $tweet_data = $tweet['tweet_data'];

      $tweet['synced_time'] = $current_time;
      $tweet['langcode'] = $real_remote_tweet['lang'];
      $tweet['tweet_time'] = strtotime($remote_tweet['created_at']);
      $tweet['user_id'] = $real_remote_tweet['user']['id_str'];
      $tweet['tweet_data'] = $remote_tweet;

      if (empty($tweet['fids'])) {
        $tweet['fids'] = $this->fetchRemoteFiles($tweet['id'], $real_remote_tweet);
      }

      $this->tweetStorage->saveTweetRecord($tweet);

      // @todo Do not clear caches if not necessary.
      // Calculate $checksum = Crypt::hashBase64(serialize($data));
      $tag = 'tweet_reference:' . $tweet['id'];
      $cache_tags[$tag] = $tag;

      if (!empty($tweet['entity_type']) && !empty($tweet['entity_id'])) {
        $tag = $tweet['entity_type'] . ':' . $tweet['entity_id'];
        $cache_tags[$tag] = $tag;
      }
    }
  }

  /**
   * Deletes tweets having common tweet identifier.
   */
  protected function deleteTweetsByTweetId($tweet_id, &$cache_tags) {
    $tweets = $this->tweetStorage->loadTweetRecordsByTweetId($tweet_id);

    foreach ($tweets as $tweet) {
      $this->tweetStorage->deleteTweetRecord($tweet['id']);

      $tag = 'tweet_reference:' . $tweet['id'];
      $cache_tags[$tag] = $tag;

      if (!empty($tweet['entity_type']) && !empty($tweet['entity_id'])) {
        $tag = $tweet['entity_type'] . ':' . $tweet['entity_id'];
        $cache_tags[$tag] = $tag;
      }
    }
  }

  /**
   * Updates user records in the database.
   */
  protected function updateUserRecord($user_id, $remote_user, $current_time, &$cache_tags) {
    $user_is_changed = FALSE;
    $user = $this->tweetStorage->loadUserRecordById($user_id);

    if (empty($user)) {
      $user_is_changed = TRUE;
      $new_user_record = [
        'user_id' => $user_id,
        'name' => $remote_user['name'],
        'screen_name' => $remote_user['screen_name'],
        'synced_time' => $current_time,
        'fid' => NULL,
        'user_data' => $remote_user,
      ];

      $fid = $this->fetchRemoteUserFile($remote_user, [], NULL);

      if ($fid > 0) {
        $new_user_record['fid'] = $fid;
      }

      $this->tweetStorage->createUserRecord($new_user_record);
    }
    elseif ($user['synced_time'] < $current_time) {
      $fid = $this->fetchRemoteUserFile($remote_user, $user['user_data'], $user['fid']);

      if ($fid < 0) {
        // Mark image as failed.
        $user['fid'] = NULL;
      }
      elseif ($fid > 0) {
        $user['fid'] = $fid;
        $user_is_changed = TRUE;
      }

      if (($user['name'] !== $remote_user['name']) || ($user['screen_name'] !== $remote_user['screen_name'])) {
        $user_is_changed = TRUE;
      }

      $user['name'] = $remote_user['name'];
      $user['screen_name'] = $remote_user['screen_name'];
      $user['synced_time'] = $current_time;
      $user['user_data'] = $remote_user;

      $this->tweetStorage->updateUserRecord($user);
    }

    if ($user_is_changed) {
      foreach ($this->tweetStorage->getTweetEntityCacheTagsByUser($user_id) as $cache_tag) {
        $cache_tags[$cache_tag] = $cache_tag;
      }
    }
  }

  /**
   * Tries to load image files to be shown in tweets.
   */
  protected function fetchRemoteFiles($id, $remote_tweet) {
    $urls = $this->getRemoteTweetImageUrls($remote_tweet);

    if (empty($urls)) {
      $urls = $this->getRemoteTweetPageImageUrls($remote_tweet);
    }

    if (empty($urls)) {
      return '[]';
    }

    /** @var \Drupal\Core\File\FileSystemInterface $fileSystem */
    $fileSystem = \Drupal::service('file_system');

    /** @var \Drupal\file\FileUsage\FileUsageInterface $file_usage */
    $file_usage = \Drupal::service('file.usage');

    $hash = $id % 100;
    $directory = "public://tweet-reference/tweet/{$hash}/";

    $delta = 0;
    $fids = [];

    foreach ($urls as $original_url => $url) {
      $filename = $id . '_' . $delta;
      $delta++;
      $original_url = preg_replace('/\?.*/isu', '', $original_url);
      $dot_pos = strrpos($original_url, '.');

      if ($dot_pos !== FALSE) {
        $extension = strtolower(preg_replace('/[^a-z0-9].*/iu', '', substr($original_url, $dot_pos + 1)));
        if (!empty($extension)) {
          $filename .= '.' . $extension;
        }
      }

      try {
        $response = \Drupal::httpClient()->get($url);
      }
      catch (\Exception $e) {
        continue;
      }

      if ($response->getStatusCode() != 200) {
        continue;
      }

      $file_data = $response->getBody()->__toString();

      if (empty($file_data)) {
        continue;
      }

      $fileSystem->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS);
      $file = file_save_data($file_data, $directory . $filename, FileSystemInterface::EXISTS_RENAME);
      unset($file_data);

      if (empty($file)) {
        continue;
      }

      $file_usage->add($file, 'tweet_reference', 'tweet_reference', $id);

      $fids[] = $file->id();
    }

    return '[' . implode(',', $fids) . ']';
  }

  /**
   * Tries to load image file of the user picture.
   */
  protected function fetchRemoteUserFile($remote_user, $existing_user_data, $existing_user_fid) {
    if (empty($remote_user['profile_image_url_https'])) {
      return 0;
    }

    $url = $remote_user['profile_image_url_https'];
    $existing_user_url = isset($existing_user_data['profile_image_url_https']) ? $existing_user_data['profile_image_url_https'] : '';

    if (($url === $existing_user_url) && !empty($existing_user_fid)) {
      return 0;
    }

    /** @var \Drupal\Core\File\FileSystemInterface $fileSystem */
    $fileSystem = \Drupal::service('file_system');

    /** @var \Drupal\file\FileUsage\FileUsageInterface $file_usage */
    $file_usage = \Drupal::service('file.usage');

    $user_id = $remote_user['id_str'];
    $hash = intval(substr($user_id, -2));
    $directory = "public://tweet-reference/user/{$hash}/";

    $filename = $user_id . '_' . time();
    $path = preg_replace('/\?.*/isu', '', $url);
    $dot_pos = strrpos($path, '.');

    if ($dot_pos !== FALSE) {
      $extension = strtolower(preg_replace('/[^a-z0-9].*/iu', '', substr($path, $dot_pos + 1)));
      if (!empty($extension)) {
        $filename .= '.' . $extension;
      }
    }

    try {
      $response = \Drupal::httpClient()->get($url);
    }
    catch (\Exception $e) {
      return -1;
    }

    if ($response->getStatusCode() != 200) {
      return -1;
    }

    $file_data = $response->getBody()->__toString();

    if (empty($file_data)) {
      return -1;
    }

    $fileSystem->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS);
    $file = file_save_data($file_data, $directory . $filename, FileSystemInterface::EXISTS_RENAME);
    unset($file_data);

    if (empty($file)) {
      return -1;
    }

    $file_usage->add($file, 'tweet_reference', 'tweet_reference_user', $user_id);

    return $file->id();
  }

  /**
   * Tries to collect image file URLs from the tweet.
   */
  protected function getRemoteTweetImageUrls($remote_tweet) {
    $urls = [];

    if (!empty($remote_tweet['extended_entities']['media'])) {
      $media_types = ['photo', 'animated_gif', 'video'];
      foreach ($remote_tweet['extended_entities']['media'] as $media) {
        if (!empty($media['type']) && in_array($media['type'], $media_types) && !empty($media['media_url_https'])) {
          $dot_pos = strrpos($media['media_url_https'], '.');
          if ($dot_pos === FALSE) {
            continue;
          }
          $url = substr($media['media_url_https'], 0, $dot_pos) . '?format=' . substr($media['media_url_https'], $dot_pos + 1) . '&name=small';
          $urls[$media['media_url_https']] = $url;
        }
      }
    }

    return $urls;
  }

  /**
   * Tries to collect image file URLs from the pages mentioned in the tweet.
   */
  protected function getRemoteTweetPageImageUrls($remote_tweet) {
    $urls = [];

    if (empty($remote_tweet['entities']['urls'])) {
      return $urls;
    }

    foreach ($remote_tweet['entities']['urls'] as $url_data) {
      if (!empty($url_data['expanded_url'])) {
        try {
          $response = \Drupal::httpClient()->get($url_data['expanded_url'], ['timeout' => 15]);
        }
        catch (\Exception $e) {
          continue;
        }

        if ($response->getStatusCode() != 200) {
          continue;
        }

        $html = $response->getBody()->__toString();
        $url = '';
        $matches = [];

        preg_match_all('/<meta\s+([^>]+)>/isu', $html, $matches);
        unset($html);

        if (empty($matches[1])) {
          continue;
        }
        $metas = $matches[1];

        foreach ($metas as $meta) {
          $meta = trim(str_replace(['"', "'"], ['', ''], $meta), ' /');
          if (preg_match('/(name|property)=twitter:image/isu', $meta) && preg_match('/content=([^\s]+)/isu', $meta, $matches)) {
            $url = Html::decodeEntities(trim($matches[1]));
            break;
          }
        }

        if (!empty($url)) {
          $urls[$url] = $url;
          // One image is enough.
          break;
        }
      }
    }

    return $urls;
  }

  /**
   * Returns the current content language.
   */
  protected static function getContentLanguage() {
    static $content_langcode = NULL;

    if (empty($content_langcode)) {
      $content_langcode = \Drupal::languageManager()->getCurrentLanguage(LanguageInterface::TYPE_CONTENT)->getId();
    }

    return $content_langcode;
  }

}
