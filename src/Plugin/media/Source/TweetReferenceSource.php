<?php

namespace Drupal\tweet_reference\Plugin\media\Source;

use Drupal\media\MediaInterface;
use Drupal\media\MediaSourceBase;

/**
 * Tweet reference media source.
 *
 * @MediaSource(
 *   id = "tweet_reference",
 *   label = @Translation("Tweet reference"),
 *   description = @Translation("Tweet reference media source"),
 *   allowed_field_types = {"tweet_reference"},
 *   default_thumbnail_filename = "no-thumbnail.png"
 * )
 */
class TweetReferenceSource extends MediaSourceBase {

  /**
   * {@inheritdoc}
   */
  public function getMetadataAttributes() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getMetadata(MediaInterface $media, $attribute_name) {
    if ($attribute_name == 'thumbnail_uri') {
      return parent::getMetadata($media, $attribute_name);
    }

    return NULL;
  }

}
