<?php

namespace Drupal\tweet_reference\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\StringTextfieldWidget;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'tweet_reference' widget.
 *
 * @FieldWidget(
 *   id = "tweet_reference",
 *   label = @Translation("Text field"),
 *   field_types = {
 *     "tweet_reference"
 *   },
 * )
 */
class TweetReferenceWidget extends StringTextfieldWidget {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    $element['target_id'] = [
      '#type' => 'value',
      '#value' => isset($items[$delta]->target_id) ? $items[$delta]->target_id : NULL,
    ];

    return $element;
  }

}
