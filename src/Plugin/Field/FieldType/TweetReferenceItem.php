<?php

namespace Drupal\tweet_reference\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Defines the 'tweet_reference' field type.
 *
 * @FieldType(
 *   id = "tweet_reference",
 *   label = @Translation("Tweet reference"),
 *   description = @Translation("This field references a tweet."),
 *   list_class = "\Drupal\tweet_reference\Plugin\Field\FieldType\TweetReferenceFieldItemList",
 *   default_widget = "tweet_reference",
 *   default_formatter = "tweet_reference"
 * )
 */
class TweetReferenceItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    return [
      'check_user' => FALSE,
      'check_counts' => FALSE,
    ] + parent::defaultFieldSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties = [];

    $properties['value'] = DataDefinition::create('string')
      ->setLabel(t('Tweet ID'))
      ->setRequired(TRUE);

    $properties['target_id'] = DataDefinition::create('integer')
      ->setLabel(t('Tweet reference ID'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'value' => [
          'type' => 'varchar',
          'length' => 64,
        ],
        'target_id' => [
          'type' => 'int',
          'unsigned' => TRUE,
        ],
      ],
      'indexes' => [
        'target_id' => ['target_id'],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    $element = [];
    $settings = $this->getSettings();

    $element['check_user'] = [
      '#type' => 'checkbox',
      '#title' => t('Check tweet user data change'),
      '#default_value' => isset($settings['check_user']) ? $settings['check_user'] : '',
      '#description' => t('The flag indicating whether entity caches should be invalidated on tweet author counts (such as number of followers) change.'),
    ];

    $element['check_counts'] = [
      '#type' => 'checkbox',
      '#title' => t('Check tweet counts change'),
      '#default_value' => isset($settings['check_counts']) ? $settings['check_counts'] : '',
      '#description' => t('The flag indicating whether entity caches should be invalidated on tweet counts (such as number of replies) change.'),
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function getConstraints() {
    $constraint_manager = \Drupal::typedDataManager()->getValidationConstraintManager();
    $constraints = parent::getConstraints();

    $constraints[] = $constraint_manager->create('ComplexData', [
      'value' => [
        'Length' => [
          'max' => 64,
          'maxMessage' => t('%name: the tweet ID can not be longer than @max characters.', ['%name' => $this->getFieldDefinition()->getLabel(), '@max' => 64]),
        ],
      ],
    ]);

    return $constraints;
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
    $values['value'] = strval(mt_rand(10000, 99999));
    return $values;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    return ($this->value === NULL) || (trim($this->value) === '');
  }

  /**
   * {@inheritdoc}
   */
  public function preSave() {
    if ($this->value !== NULL) {
      $this->value = trim($this->value);
    }

    if (!$this->getEntity()->isDefaultRevision()) {
      return;
    }

    if (!empty($this->value)) {
      $this->target_id = static::getTweetStorage()->createEmptyTweetRecordIfNotExists($this->target_id, $this->value);
    }
    elseif (!empty($this->target_id)) {
      static::getTweetStorage()->deleteTweetRecord($this->target_id);
      $this->target_id = NULL;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function postSave($update) {
    parent::postSave($update);

    /** @var \Drupal\Core\Entity\SynchronizableInterface $entity */
    $entity = $this->getEntity();
    $settings = $this->getSettings();

    if (!empty($this->target_id)) {
      static::getTweetStorage()->updateTweetRecord($this->target_id, $this->value, $entity->getEntityTypeId(), $entity->id(), !empty($settings['check_user']), !empty($settings['check_counts']), !$entity->isSyncing());
    }
  }

  /**
   * Returns the tweet storage service.
   *
   * @return \Drupal\tweet_reference\TweetStorage
   */
  protected static function getTweetStorage() {
    return \Drupal::service('tweet_reference.tweet_storage');
  }

}
