<?php

namespace Drupal\tweet_reference\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemList;

/**
 * Represents a tweet reference field.
 */
class TweetReferenceFieldItemList extends FieldItemList {

  /**
   * {@inheritdoc}
   */
  public function postSave($update) {
    parent::postSave($update);

    $entity = $this->getEntity();

    if (!$update || !$entity->isDefaultRevision()) {
      return;
    }

    /** @var \Drupal\Core\Entity\FieldableEntityInterface $original */
    $original = $entity->original;
    $langcode = $this->getLangcode();

    if ($original->hasTranslation($langcode)) {
      $field_name = $this->getFieldDefinition()->getName();
      $original_items = $original->getTranslation($langcode)->{$field_name};
      $ids = $this->getReferencedIds();
      $original_ids = $this->getReferencedIds($original_items);
      $removed_ids = array_filter(array_diff($original_ids, $ids));
      static::getTweetStorage()->deleteTweetRecords($removed_ids);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function delete() {
    parent::delete();

    $entity = $this->getEntity();

    if ($entity->isDefaultTranslation()) {
      // Delete all tweets associated with this entity.
      static::getTweetStorage()->deleteTweetRecordsForEntity($entity->getEntityTypeId(), $entity->id());
    }
    else {
      // Delete all tweets associated with this translation.
      static::getTweetStorage()->deleteTweetRecords($this->getReferencedIds());
    }
  }

  /**
   * Returns the list of referenced tweet records.
   */
  protected function getReferencedIds(FieldItemList $tweet_item_list = NULL) {
    if (empty($tweet_item_list)) {
      $tweet_item_list = $this;
    }

    $ids = [];

    foreach ($tweet_item_list->list as $item) {
      if (!empty($item->target_id)) {
        $ids[$item->target_id] = $item->target_id;
      }
    }

    return array_values($ids);
  }

  /**
   * Returns the tweet storage service.
   *
   * @return \Drupal\tweet_reference\TweetStorage
   */
  protected static function getTweetStorage() {
    return \Drupal::service('tweet_reference.tweet_storage');
  }

}
