<?php

namespace Drupal\tweet_reference\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin implementation of the 'tweet_reference' formatter.
 *
 * @FieldFormatter(
 *   id = "tweet_reference",
 *   label = @Translation("Default"),
 *   field_types = {
 *     "tweet_reference"
 *   }
 * )
 */
class TweetReferenceFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function prepareView(array $entities_items) {
    $ids = [];

    foreach ($entities_items as $items) {
      foreach ($items as $item) {
        $ids[] = $item->target_id;
      }
    }

    if (empty($ids)) {
      return;
    }

    $tweets = static::getTweetStorage()->loadExtTweetRecordsById($ids);

    foreach ($entities_items as $items) {
      foreach ($items as $item) {
        if (isset($tweets[$item->target_id])) {
          $item->tweet = $tweets[$item->target_id];
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {
      $elements[$delta] = [
        '#cache' => [
          'tags' => ['tweet_reference:' . $item->target_id],
          'contexts' => ['languages:language_content', 'languages:language_interface'],
        ],
      ];

      if (!isset($item->tweet) || empty($item->tweet['tweet_data'])) {
        continue;
      }

      $fids_str = !empty($item->tweet['fids']) ? $item->tweet['fids'] : '';
      $fids = [];

      foreach (explode(',', $fids_str) as $fid) {
        $fid = intval(trim($fid, ' []'));

        if (empty($fid)) {
          continue;
        }
        $fids[] = $fid;
      }

      $entity = $items->getEntity();
      $entity_type = $entity->getEntityTypeId();

      $elements[$delta]['#theme'] = 'tweet_reference';
      $elements[$delta]['#tweet_id'] = $item->tweet['tweet_id'];
      $elements[$delta]['#fids'] = $fids;
      $elements[$delta]['#user_fid'] = !empty($item->tweet['user_fid']) ? $item->tweet['user_fid'] : NULL;
      $elements[$delta]['#tweet'] = $item->tweet['tweet_data'];
      $elements[$delta]['#container'] = 'entity_field';
      $elements[$delta]['#context'] = [
        'view_mode' => $this->viewMode,
        'language' => $items->getLangcode(),
        'field_name' => $items->getFieldDefinition()->getName(),
        'field_type' => $items->getFieldDefinition()->getType(),
        'entity_type' => $entity_type,
        'bundle' => $entity->bundle(),
        'entity' => $items->getEntity(),
      ];
    }

    return $elements;
  }

  /**
   * Returns the tweet storage service.
   *
   * @return \Drupal\tweet_reference\TweetStorage
   */
  protected static function getTweetStorage() {
    return \Drupal::service('tweet_reference.tweet_storage');
  }

}
